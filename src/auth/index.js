/* eslint-disable */
import Cookies from 'universal-cookie';
const cookies = new Cookies();
export default {
    login (token) {
        token = token.split("&");
        let expires = Date.now();
        expires += parseInt(token[2].split("=")) * 1000;
        expires = new Date(expires);

        cookies.set(
            "pariwo",
            token,
            {
                path: "/",
                expires: expires
            }
            );
    },

    getToken () {
        let token = cookies.get('pariwo');
        return token[0];
    },

    logout () {
        document.cookie = 'pariwo=; Max-Age=-99999999;';
    },

    loggedIn () {
        const token = cookies.get('pariwo');
        return (token)
    },
}
