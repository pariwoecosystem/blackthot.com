import Vue from 'vue'
import Router from 'vue-router'
import VueCookies from 'vue-cookies'
import BootstrapVue from 'bootstrap-vue'
// import BlackThotHome from '@/components/BlackThotHome'
import BlackThotPost from '@/components/posts/BlackThotPost'
import BlackThotLogin from '@/components/BlackThotLogin'
import BlackThotWrite from '@/components/BlackThotWrite'
import BlackThotHome from '@/components/BlackThotHome'
import BlackThotDashboard from '@/components/BlackThotDashboard'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../assets/main.css'
Vue.use(BootstrapVue);
Vue.use(Router);
Vue.use(VueCookies);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'HomePage',
            component: BlackThotHome
        },
        {
            path: '/login',
            name: 'login',
            component: BlackThotLogin
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: BlackThotDashboard
        },
        {
            path: '/logout',
            name: 'logout',
            component: BlackThotLogin
        },
        {
            path: '/write',
            name: 'write',
            component: BlackThotWrite
        },
        {
            path: '/post/:id',
            name: 'thot',
            component: BlackThotPost,
            props: true
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: BlackThotWrite,
            props: true
        }
    ]
})
