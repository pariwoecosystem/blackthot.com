'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
    CLIENT_BASE_URL: '"http://localhost:8080"',
    API_CLIENT_TOKEN: '"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNkNGUwMWUzNjU5ZTViYWJlNTQyYTg0OGM1NDM0OWE1NDBjOTIxZmYzNDlhN2YyNjMzOGU1ZWI1YjdiMDcwMTRjZjllNzM2ZTUzMDQ4MjMwIn0.eyJhdWQiOiI2IiwianRpIjoiM2Q0ZTAxZTM2NTllNWJhYmU1NDJhODQ4YzU0MzQ5YTU0MGM5MjFmZjM0OWE3ZjI2MzM4ZTVlYjViN2IwNzAxNGNmOWU3MzZlNTMwNDgyMzAiLCJpYXQiOjE1MjkzMDQ5OTIsIm5iZiI6MTUyOTMwNDk5MiwiZXhwIjoxNTYwODQwOTkyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.iokKHVzsQYW7p1H3eS2gHWS9nHjKuUKx2obgnafmv5ILZisQjrdK4jyj7tJmc995sbP14VWMgwNmNTNJEPkN_-650Z09Y8io_InnZzYqEJUH16KToUmYMkJI_I3kFhFDS5P9hsl6x6jmLMqWdNY86cEVd6zs5YjGq1EMZxuia91gCVegZKXW7HUaFIrUnVA8TjsNHnxHgdUm5uhFeDdSoerRfTGCmvG1Tipr5QF1TNLWgXp77dohat-0e-ajIGKMfZoLeBmCoIhJv8evgEBQ78hKCBWxgsn6fU5QJoBuhJIKhxqpV3goklrdp-1gFZcE_MzJSgGWz8qiNQ0kHImXujwNy0AGJsWAJICTBeEi8cZ5676DXyYTrDOJfqkPBa9CbYS3i78YPp9lCsqmtGt7wkUp_kQnQDxu0XPGQ4egujmupXWs92KS4w_TEuujhvRxTTrAc-nbUAJGMlC7JjXH3SePXUzj96JccjSPoB5tqdo1tQpsWaHr1Bz0I6-0pbmhaGUzeb5g6st8t2B4sYY0zhKuXiSSsABieoCmntIqE5n4R4g1qxiJtZRrAKMVJ7G7pXGc-uSKH7ziJpn6OyGqzei51_2O5-8iJTKXpmzi2dexSEJToCrbUKSU-Yu3FBuaoVzyfo_dye02ZlGdYqKidymA0CFs89ek6qm3uQW6wQo"',
    ALABO_URL: '"http://alabo.pariwo.ann/"',
    PARIWO_URL: '"http://pariwo.ann/"',
    API_URL: '"http://okan.pariwo.ann/"',
    REDIRECT_URL: '"http://localhost:8080/login"',
    PLATFORM: '"?platform=10"',
    CLIENT_ID: 6,
    PLATFORM_ID: 10,
    COLLECTION_ID: 11,
})
